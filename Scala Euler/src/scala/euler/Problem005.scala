package scala.euler

import scala.euler.common.Performance

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
object Problem005 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      println(s"Result: ${kgV((1 to 20).toList)}")

      def kgV(a: List[Int]): Int = {
        if (a.size == 1) {
          a.head
        } else {
          a.head * kgV(a.tail.map {
            e =>
              if (e % a.head == 0) {
                e / a.head
              } else {
                e
              }
          })
        }
      }
    }
  }

}