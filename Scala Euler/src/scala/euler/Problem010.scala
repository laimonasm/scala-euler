package scala.euler

import scala.euler.common.Performance
import scala.euler.common.Math

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */
object Problem010 {
  def main(args: Array[String]): Unit = {
    Performance.executionTime {
      println(s"Answare: ${(2L until 2000000).filter(x => Math.isPrime(x)).sum}")
    }
  }
}