package scala.euler

import scala.euler.common.{Math, Performance}

/**
 * n! means n × (n − 1) × ... × 3 × 2 × 1
 * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 * Find the sum of the digits in the number 100!
 */

object Problem020 {

  def main(args: Array[String]): Unit = {

    Performance.executionTime {
      println(Math.factorial(100).toString().toList.map(i => i.asDigit).sum)
    }
  }
}