package scala.euler

import scala.euler.common.Performance

/**
 * You are given the following information, but you may prefer to do some research for yourself.
 *1 Jan 1900 was a Monday.
 *Thirty days has September,
 *April, June and November.
 *All the rest have thirty-one,
 *Saving February alone,
 *Which has twenty-eight, rain or shine.
 *And on leap years, twenty-nine.
 *A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
 *How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */

object Problem019 {

  def main(args: Array[String]): Unit = {

    Performance.executionTime {

      val lengths = Array(31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

      val ls = for(y <- 1900 to 2000; m <- 1 to 12) yield
        if(m == 2)
          if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0)) 29 else 28
        else
          lengths(m - 1)

      val fs = ls.scanLeft(1)((ws, l) => (ws + l) % 7)

      val r = fs.drop(12).take(1200).count(_ == 0)

      assert(r == 171)
    }
  }
}