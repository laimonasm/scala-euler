package scala.euler

import scala.euler.common.Performance
import scala.collection.mutable

/**
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
 * The use of "and" when writing out numbers is in compliance with British usage.
 */

object Problem017 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      var letterCount = 0
      val n1 = new mutable.HashMap[Int, String]() +=(0 -> "", 1 -> "one", 2 -> "two", 3 -> "three", 4 -> "four", 5 -> "five", 6 -> "six", 7 -> "seven", 8 -> "eight", 9 -> "nine", 10 -> "ten", 11 -> "eleven", 12 -> "twelve", 13 -> "thirteen", 14 -> "fourteen", 15 -> "fifteen", 16 -> "sixteen", 17 -> "seventeen", 18 -> "eighteen", 19 -> "nineteen")
      val n2 = new mutable.HashMap[Int, String]() +=(2 -> "twenty", 3 -> "thirty", 4 -> "forty", 5 -> "fifty", 6 -> "sixty", 7 -> "seventy", 8 -> "eighty", 9 -> "ninety")

      def countLetters(i: Int) = {

        if (i >= 1 && i <= 9) letterCount += n1(i).length
        else if (i >= 10 && i <= 19) letterCount += n1(i).length
        else if (i >= 20 && i <= 99) letterCount += (n2(i / 10).length + n1(i % 10).length)
        else if (i >= 100 && i <= 999) {
          if (i % 100 == 0) {
            letterCount += n1(i / 100).length + " hundred".replace(" ", "").length
          }
          else {
            if (i % 100 > 19)
              letterCount += n1(i / 100).length + " hundred and ".replace(" ", "").length + n2((i % 100) / 10).length + n1(i % 10).length
            else {
              letterCount += n1(i / 100).length + " hundred and ".replace(" ", "").length + n1(i % 100).length
            }
          }
        }
        else {
          letterCount += "one thousand".replace(" ", "").length
        }
      }

      for (x <- 1 to 1000) {
        countLetters(x)
      }

      println(s"Answare: $letterCount")
    }
  }
}