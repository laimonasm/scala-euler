package scala.euler

import scala.euler.common.Performance
import scala.euler.common.Math

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 */
object Problem003 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {
      val maxPrimeFactor = Math.factors(600851475143L).max;
      println(s"Maximum prime factor: $maxPrimeFactor")
    }
  }
}