package scala.euler

import scala.euler.common.Performance
import scala.euler.common.Math

/**
 * The following iterative sequence is defined for the set of positive integers:
 * n → n/2 (n is even)
 * n → 3n + 1 (n is odd)
 * Using the rule above and starting with 13, we generate the following sequence:
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet
 * (Collatz Problem), it is thought that all starting numbers finish at 1.
 * Which starting number, under one million, produces the longest chain?
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */

object Problem014 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      def getSequenceCount(x: Long): Int = {
        var temp = x
        var counter = 1
        while (temp != 1) {
          if (Math.isEvenNumber(temp)) temp = Math.even(temp)
          else temp = Math.odd(temp)

          counter = counter + 1
        }

        counter
      }

      var startingNumber = 0
      var maxSequenceCount = 0

      for (x <- 1 to 1000000) {
        val currentSequenceCount = getSequenceCount(x)
        if (currentSequenceCount > maxSequenceCount) {
          maxSequenceCount = currentSequenceCount
          startingNumber = x
        }
      }

      println(s"Answare:" + startingNumber)
    }
  }
}