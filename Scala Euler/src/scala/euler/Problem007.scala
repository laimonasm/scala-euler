package scala.euler

import scala.euler.common.Math
import scala.euler.common.Performance

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 */
object Problem007 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      var bContinueCycle = true
      var iCounter = 2
      var iCounterPrimes = 0

      while (bContinueCycle) {
        if (Math.isPrime(iCounter)) {
          iCounterPrimes += 1

          if (iCounterPrimes == 10001) {
            bContinueCycle = false
            println(s"Answare: $iCounter");
          }
        }

        iCounter += 1
      }
    }
  }

}