package scala.euler

import scala.euler.common.Performance
import scala.util.control.Breaks._

/**
 * A Pythagorean triplet is a set of three natural numbers, a  b  c, for which, a2 + b2 = c2
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 *
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */
object Problem009 {
  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      breakable {
        for (a <- 1 to 500)
          for (b <- 1 to 500) {
            val c = math.sqrt(math.pow(a, 2) + math.pow(b, 2));

            if (c.isValidInt) {
              if (a + b + c == 1000) {
                println("a = " + a + " b = " + b + " c = " + c.toInt)
                println(s"Answare a*b*c: ${a * b * c.toInt}")
                break
              }
            }
          }
      }
    }
  }
}