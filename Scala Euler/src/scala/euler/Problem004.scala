package scala.euler

import scala.euler.common.Performance
import scala.euler.common.Math

/**
 * A palindromic number reads the same both ways.
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
object Problem004 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {
      val maxThreeDigistPolindromicNumbers = for (x <- 900 to 999; y <- 900 to 999 if (Math.isPalindrome(x * y))) yield x * y
      println(s"Max palindromic number of 3 digits is: ${maxThreeDigistPolindromicNumbers.max}")
    }
  }
}