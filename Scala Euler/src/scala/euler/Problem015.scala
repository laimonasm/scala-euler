package scala.euler

import scala.euler.common.Performance
import scala.euler.common.Math

/**
 * Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down,
 * there are exactly 6 routes to the bottom right corner.
 * How many such routes are there through a 20×20 grid?
 */

object Problem015 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      val pathCount = Math.factorial(40) / (Math.factorial(20) * Math.factorial(20))
      println(s"Answare: $pathCount")
    }
  }
}