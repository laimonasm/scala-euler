package scala.euler

import scala.euler.common.Performance

/**
 * The sum of the squares of the first ten natural numbers is, 1^2 + 2^2 + ... + 10^2 = 385
 * The square of the sum of the first ten natural numbers is, (1 + 2 + ... + 10)^2 = 55^2 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 - 385 = 2640.
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */
object Problem006 {

  def main(args: Array[String]): Unit = {
    Performance.executionTime {

      val squareOfSums = math.pow((1 until 101).sum, 2)
      var sumOfSquares: Double = 0;
      (1 until 101).foreach(x => sumOfSquares += math.pow(x, 2))
      println(s"Answare: ${(squareOfSums - sumOfSquares).toInt}");
    }
  }

}