package scala.euler.common

import scala.math.BigInt

object Math {

  /**
   * Check if prime number
   */
  def isPrime(num: Long) = {
    !(2L to math.sqrt(num).toInt).exists(num % _ == 0)
  }

  /**
   * Sieving integral numbers
   */
  def sieve(s: Stream[Int]): Stream[Int] = {
    s.head #:: sieve(s.tail.filter(_ % s.head != 0))
  }

  /**
   * Calculate Fibonacci number
   */
  def fibonacci(x: BigInt, y: BigInt): Stream[BigInt] = {
    x #:: fibonacci(y, x + y)
  }

  /**
   * Get number all factors
   */
  def factors(x: Long): List[Long] = {
    val exists = (2L to math.sqrt(x).toLong).find(x % _ == 0)
    exists match {
      case Some(y) => y :: factors(x / y)
      case None => List(x)
    }
  }

  /**
   * Get number all factorial
   */
  def factorial(n: BigInt): BigInt = if (n == 0) 1 else n * factorial(n - 1)

  /**
   * Check if number is polindromic
   */
  def isPalindrome(x: Int): Boolean = {
    x.toString == augmentString(x.toString).reverse
  }

  /**
   * Get triangle number
   */
  def triangleNumberByIndex(x: Long): Long = {
    x * (x + 1) / 2
  }

  /**
   * Get number divisors count using factors
   */
  def getNumberDivisorsCount(x: Long): Long = {
    var divisorsCount = 1
    val factorsGrouped = factors(x).groupBy(y => y)
    factorsGrouped.foreach(factor => {
      divisorsCount = divisorsCount * (factor._2.length + 1)
    })

    divisorsCount
  }

  def even(x: Long): Long = {
    x / 2
  }

  def odd(x: Long): Long = {
    3 * x + 1
  }

  def isEvenNumber(x: Long): Boolean = {
    x % 2 == 0
  }
}