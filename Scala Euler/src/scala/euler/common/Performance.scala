package scala.euler.common

object Performance {

  /**
   * Calculate executed block time in milliseconds
   */
  def executionTime[R](block: => R): R = {
    val timeStart = System.currentTimeMillis()
    val result = block
    val timeEnd = System.currentTimeMillis()
    val timeElapsed = (timeEnd - timeStart)
    println(s"Elapsed time: ${timeElapsed}ms")
    result
  }

}